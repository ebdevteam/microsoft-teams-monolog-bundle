<?php
/**
 * Copyright (c) 2019. Exclusive Books Group (Pty) Ltd. All Rights Reserved.
 */

namespace ExclusiveBooks\MicrosoftTeamsMonologBundle\Logger;

use ExclusiveBooks\MicrosoftTeamsMonologBundle\Handlers\TeamsProcessingHandler;
use Monolog\Logger;

/**
 * Class TeamsLogger.
 *
 * @author Francois Pienaar <francoisp@exclusivebooks.co.za>
 */
final class TeamsLogger extends Logger
{
    /**
     * TeamsLogger constructor.
     *
     * @param string $url    The incoming web-hook URL
     * @param int    $level  The level of logging this handler will handle
     * @param bool   $bubble Does the logging bubble
     */
    public function __construct($url, $level = Logger::DEBUG, $bubble = true)
    {
        parent::__construct('teams');

        $this->pushHandler(new TeamsProcessingHandler($url, $level, $bubble));
    }
}
