<?php
/**
 * Copyright (c) 2019. Exclusive Books Group (Pty) Ltd. All Rights Reserved.
 */

namespace ExclusiveBooks\MicrosoftTeamsMonologBundle\Handlers;

use ExclusiveBooks\MicrosoftTeamsMonologBundle\Model\MessageCard;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use function curl_init;

/**
 * Class TeamsProcessingHandler.
 *
 * @author Francois Pienaar <francoisp@exclusivebooks.co.za>
 */
final class TeamsProcessingHandler extends AbstractProcessingHandler
{
    /**
     * @var string
     */
    private $url;

    /**
     * TeamsProcessingHandler constructor.
     *
     * @param string $url
     * @param int    $level
     * @param bool   $bubble
     */
    public function __construct($url, $level = Logger::DEBUG, $bubble = true)
    {
        parent::__construct($level, $bubble);
        $this->url = $url;
    }

    /**
     * Writes the record down to the log of the implementing handler.
     *
     * @param array $record
     */
    protected function write(array $record)
    {
        $messageJson = json_encode(new MessageCard($record['level'], $record['level_name'], $record['message']));
        $curlHandler = curl_init($this->url);
        curl_setopt($curlHandler, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $messageJson);
        curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandler, CURLOPT_TIMEOUT, 10);
        curl_setopt($curlHandler, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt(
            $curlHandler,
            CURLOPT_HTTPHEADER,
            [
                'Content-Type: application/json',
                'Content-Length: '.strlen($messageJson),
            ]
        );

        curl_exec($curlHandler);
    }
}
