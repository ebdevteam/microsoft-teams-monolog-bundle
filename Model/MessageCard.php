<?php
/**
 * Copyright (c) 2019. Exclusive Books Group (Pty) Ltd. All Rights Reserved.
 */

namespace ExclusiveBooks\MicrosoftTeamsMonologBundle\Model;

use JsonSerializable;
use Monolog\Logger;

/**
 * Class MessageCard.
 *
 * @author Francois Pienaar <francoisp@exclusivebooks.co.za>
 *
 * @see    https://docs.microsoft.com/outlook/actionable-messages/message-card-reference
 */
class MessageCard implements JsonSerializable
{
    const GREY = 'EDEFF2';
    const RED = 'FF0000';
    /** @var array Map log levels to specific colours */
    private static $colorMap = [
        Logger::DEBUG => self::GREY,
        Logger::INFO => self::GREY,
        Logger::NOTICE => self::GREY,
        Logger::WARNING => 'FF8000',
        Logger::ERROR => self::RED,
        Logger::CRITICAL => self::RED,
        Logger::ALERT => self::RED,
        Logger::EMERGENCY => self::RED,
    ];
    private static $context = 'http://schema.org/extensions';
    private static $type = 'MessageCard';
    /**
     * @var string
     */
    public $themeColor;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $text;

    /**
     * MessageCard constructor.
     *
     * @param int    $level
     * @param string $title
     * @param string $text
     */
    public function __construct($level, $title, $text)
    {
        $this->themeColor = self::$colorMap[$level];
        $this->title = $title;
        $this->text = $text;
    }

    /**
     * Specify data which should be serialized to JSON.
     *
     * @see   https://php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed data which can be serialized by <b>json_encode</b>,
     *               which is a value of any type other than a resource
     *
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            '@context' => $this::$context,
            '@type' => $this::$type,
            'themeColor' => $this->themeColor,
            'title' => $this->title,
            'text' => $this->text,
        ];
    }
}
