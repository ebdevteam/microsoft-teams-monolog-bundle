<?php
/**
 * Copyright (c) 2019. Exclusive Books Group (Pty) Ltd. All Rights Reserved.
 */

namespace ExclusiveBooks\MicrosoftTeamsMonologBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ExclusiveBooksMicrosoftTeamsMonologBundle.
 */
class ExclusiveBooksMicrosoftTeamsMonologBundle extends Bundle
{
}
