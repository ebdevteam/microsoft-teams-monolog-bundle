<?php
/**
 * Copyright (c) 2019. Exclusive Books Group (Pty) Ltd. All Rights Reserved.
 */

namespace ExclusiveBooks\MicrosoftTeamsMonologBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;

/**
 * Class ExclusiveBooksMicrosoftTeamsMonologExtension.
 *
 * @author Francois Pienaar <francoisp@exclusivebooks.co.za>
 */
class ExclusiveBooksMicrosoftTeamsMonologExtension extends ConfigurableExtension
{
    /**
     * @return string
     */
    public function getNamespace()
    {
        return 'https://exclusivebooks.co.za/schema/microsoft-teams-monolog';
    }

    /**
     * @return bool|string
     */
    public function getXsdValidationBasePath()
    {
        return __DIR__.'/../Resources/config/schema';
    }

    /**
     * Configures the passed container according to the merged configuration.
     *
     * @param array                                                   $mergedConfig
     * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
     *
     * @throws \Exception
     */
    protected function loadInternal(array $mergedConfig, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(dirname(__DIR__).'/Resources/config'));
        $loader->load('services.yml');

        foreach ($mergedConfig['channels'] as $channel => $settings) {
            $definition = $container->getDefinition('exclusive_books.monolog.microsoft_teams');
            $definition->replaceArgument(0, $settings['url']);
            $definition->replaceArgument(1, $settings['level']);
        }
    }
}
