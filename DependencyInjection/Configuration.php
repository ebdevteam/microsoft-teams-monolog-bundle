<?php
/**
 * Copyright (c) 2019. Exclusive Books Group (Pty) Ltd. All Rights Reserved.
 */

namespace ExclusiveBooks\MicrosoftTeamsMonologBundle\DependencyInjection;

use Monolog\Logger;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration.
 *
 * @author Francois Pienaar <francoisp@exclusuivebooks.co.za>
 */
class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('exclusive_books_microsoft_teams_monolog');
        $rootNode = method_exists(TreeBuilder::class, 'getRootNode') ? $treeBuilder->getRootNode() : $treeBuilder->root(
            'exclusive_books_microsoft_teams_monolog'
        );

        $rootNode
            ->fixXmlConfig('channel')
                ->children()
                    ->arrayNode('channels')
                        ->useAttributeAsKey('name')
                        ->arrayPrototype()
                            ->children()
                                ->scalarNode('url')
                                    ->isRequired()
                                    ->cannotBeEmpty()
                                ->end() // end URL
                                ->scalarNode('level')
                                    ->defaultValue(Logger::DEBUG)
                                ->end() // end level
                            ->end() // end prototype children
                        ->end() //end prototype
                    ->end() //end 'channels'
                ->end() //end 'channel'
        ;

        return $treeBuilder;
    }
}
